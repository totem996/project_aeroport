#include "mainform.h"
#include "ui_mainform.h"
#include <QString>
#include <QFile>
#include <QTextCodec>
#include <QTextStream>
#include "QTime"

MainForm::MainForm(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::MainForm)
{
    ui->setupUi(this);
    QTextCodec *codec = QTextCodec::codecForName("UTF8");
    QTextCodec::setCodecForLocale(codec);

    QTime midnight(0,0,0);
    qsrand(midnight.secsTo(QTime::currentTime()));

    // генерация городов
    city[0].title = "София";
    city[0].x = 3308;
    city[0].y = 7887;
    city[1].title = "Париж";
    city[1].x = 3855;
    city[1].y = 5259;
    city[2].title = "Нью-Йорк";
    city[2].x = 4777;
    city[2].y = 1523;
    city[3].title = "Москва";
    city[3].x = 718;
    city[3].y = 4176;
    city[4].title = "Милан";
    city[4].x = 7378;
    city[4].y = 7326;
    city[5].title = "Минск";
    city[5].x = 8912;
    city[5].y = 6193;
    city[6].title = "Прага";
    city[6].x = 6086;
    city[6].y = 8593;
    city[7].title = "Венеция";
    city[7].x = 1562;
    city[7].y = 3137;
    city[8].title = "Амстердам";
    city[8].x = 3260;
    city[8].y = 6825;
    city[9].title = "Лондон";
    city[9].x = 7304;
    city[9].y = 9723;
    city[10].title = "Вена";
    city[10].x = 5747;
    city[10].y = 7359;
    city[11].title = "Рим";
    city[11].x = 2351;
    city[11].y = 5593;
    city[12].title = "Шанхай";
    city[12].x = 1627;
    city[12].y = 4834;

    Generation(city, fls, n); // генерация рейсов
    Print(fls, n);
}

void MainForm::Generation(struct_city* city, struct_fls* fls, int n)
{
    int a, b;
    int i = 0;
    while (i < n)
    {
        a = qrand() % 13;
        b = qrand() % 13;
        if (a != b)
        {
            fls[i].title = city[qrand() % 13].title + " - " + city[qrand() % 13].title;
            if (qrand() % 2 == 1)
                fls[i].type1 = "регулярный, мин. частота перелёта: " + QString::number(2 + qrand() % 6);
            else
                fls[i].type1 = "разовый";
            if (qrand() % 2 == 1)
                fls[i].type2 = "грузовой";
            else
                fls[i].type2 = "пассажирский";
            fls[i].price = qrand() % 100000 + 3000;
            fls[i].penalty = fls[i].price / 100 * 40;
            if (fls[i].type1 != "разовый")
            {
                int a = qrand() % 4;
                if (a == 1)
                    fls[i].good_time = "утренние";
                if (a == 2)
                    fls[i].good_time = "дневные";
                if (a == 3)
                    fls[i].good_time = "вечерние";
                if (a == 0)
                    fls[i].good_time = "ночные";
            }
            else
                fls[i].good_time = "нет";
            fls[i].range = abs(sqrt((city[b].x-city[a].x)*(city[b].x-city[a].x)+(city[b].y-city[a].y)*(city[b].y-city[a].y)));
            i++;
            }
    }
}

void MainForm::Print(struct_fls *fls, int n)
{
    int i = 0;
    while(i < n)
    {
        new_fls = new QStandardItem(QString(fls[i].title));
        fls_model->setItem(i, 0, new_fls);
        new_fls = new QStandardItem(QString("Рейс '" + fls[i].title + "': " + fls[i].type1 + ", " + fls[i].type2 + "\nМинимальная стоимость: " + QString::number(fls[i].price) + " рублей\nДальность: " + QString::number(fls[i].range) + " км.\nНеустойка: " + QString::number(fls[i].penalty) + " рублей\nПиковые часы: " + fls[i].good_time));
        fls_model->setItem(i, 1, new_fls);
        i++;
    }
    i = 0;
    ui->tableView_flights->setModel(fls_model);
    ui->tableView_flights->setColumnHidden(1, true);
    ui->tableView_flights->setColumnHidden(2, true);
    ui->tableView_flights->setColumnWidth(0, 300);
}

MainForm::~MainForm()
{
    delete ui;
}

// при клике на рейс в таблице вывод информации о нём
void MainForm::on_tableView_flights_clicked(const QModelIndex &index)
{
    int row = ui->tableView_flights->currentIndex().row();
    ui->textEdit_information->setText(ui->tableView_flights->model()->data(ui->tableView_flights->model()->index(row, 1)).toString());
}

void MainForm::on_pushButton_2_clicked()
{
    m = 1 + qrand() % 5;
    int N = ui->tableView_flights->model()->rowCount();
    int k = 0;

    //удаление закончившихся рейсов
    while (k < N)
    {
        if(qrand() % 2 == 1) // тут как бы проверка, закончилось время жизни у рейса или нет
            ui->tableView_flights->model()->removeRow(k);
        k++;
    }

    struct_fls *fls2 = new struct_fls[m];
    Generation(city, fls2, m); // генерация рейсов
    //добавление новых рейсов
    int lastRow;
    for (int i = 0; i < m; i++)
    {
        lastRow = fls_model->rowCount();
        fls_model->insertRow(lastRow);
        fls_model->setData(fls_model->index(lastRow,0),QString(fls2[i].title));
        fls_model->setData(fls_model->index(lastRow,1),QString("Рейс '" + fls2[i].title + "': " + fls2[i].type1 + ", " + fls2[i].type2 + "\nМинимальная стоимость: " + QString::number(fls2[i].price) + " рублей\nДальность: " + QString::number(fls2[i].range) + " км.\nНеустойка: " + QString::number(fls2[i].penalty) + " рублей\nПиковые часы: " + fls2[i].good_time));
    }
}

void MainForm::on_pushButton_clicked()
{
    this->hide();
}
