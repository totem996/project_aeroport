#ifndef MAINFORM_H
#define MAINFORM_H

#include <QDialog>
#include "QStandardItemModel"
#include "QStandardItem"

namespace Ui {
class MainForm;
}

struct struct_city {
    QString title;
    int x;
    int y;
};

// рейсы
struct struct_fls {
    QString title;
    QString type1;
    QString type2;
    QString good_time;
    double penalty;
    double price;
    int range;
};

class MainForm : public QDialog
{
    Q_OBJECT

public:
    // модель - рейсы
    QStandardItemModel *fls_model = new QStandardItemModel;
    QStandardItem *new_fls;  
    struct_city *city = new struct_city[13];
    int n = 2 + qrand() % 5; //сколько будет сгенерировано рейсов cначала
    int m, l;

    struct_fls *fls = new struct_fls[n];

    void Generation(struct_city *city, struct_fls* fls, int n);
    void Print(struct_fls* fls, int n);
    explicit MainForm(QWidget *parent = 0);
    ~MainForm();

private slots:
    void on_tableView_flights_clicked(const QModelIndex &index);

    void on_pushButton_2_clicked();

    void on_pushButton_clicked();

private:
    Ui::MainForm *ui;
};

#endif // MAINFORM_H
